package com.integrimarine.integriclient.Activities;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.integrimarine.integriclient.R;
import com.integrimarine.integriclient.Services.JSONHttp;
import com.integrimarine.integriclient.Services.SharedPreferenceService;

import org.json.JSONArray;

public class LoginScreenActivity extends AppCompatActivity {


    Dialog dialog;
    int ALL_PERMISSIONS = 101;
    final String[] permissions = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
    EditText encrptedKeyValue;
    CardView activate;
    Context mContext;
    ProgressBar ProgressBarBeforeActivating;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_login_screen );
        mContext=this;
        ProgressBarBeforeActivating=findViewById( R.id.ProgressBarBeforeActivating );
        ActivityCompat.requestPermissions(this, permissions, ALL_PERMISSIONS);
        checkSystemWritePermission();
        if(SharedPreferenceService.getInstance( this ).getToken() != null){
            startActivity( new Intent( LoginScreenActivity.this,MediaScreensActivity.class ) );
            finish();
        }
        getWindow().setFlags( WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN );
        encrptedKeyValue=findViewById( R.id.encryptedKey );
        activate=findViewById( R.id.activate );
        activate.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProgressBarBeforeActivating.setVisibility( View.VISIBLE );
                login();
            }
        } );

        ActivityCompat.requestPermissions(this, permissions, ALL_PERMISSIONS);
    }

    private void login(){
        JSONHttp.getInstance( LoginScreenActivity.this ).login( encrptedKeyValue.getText().toString(), new JSONHttp.JSONHttpResponseListener() {
            @Override
            public void onResult(JSONArray response) {
                ProgressBarBeforeActivating.setVisibility( View.GONE );
                startActivity( new Intent( LoginScreenActivity.this,MediaScreensActivity.class ) );
                finish();
            }

            @Override
            public void onError() {
                ProgressBarBeforeActivating.setVisibility( View.GONE );
                Toast.makeText( LoginScreenActivity.this,"Wrong token or please check your internet connection",Toast.LENGTH_LONG ).show();
            }
        } );
    }

    private boolean checkSystemWritePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (Settings.System.canWrite(mContext)) return true;
            else openAndroidPermissionsMenu();
        }
        return false;
    }

    private void openAndroidPermissionsMenu() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
            intent.setData(Uri.parse("package:" + mContext.getPackageName()));
            mContext.startActivity(intent);
        }
    }
    /**
     * On RequestPermissionsResult to ask permission and to check permission granted or not
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        for (String permission : permissions) {
            if (ActivityCompat.shouldShowRequestPermissionRationale( this, permission )) {

            } else {
                if (ActivityCompat.checkSelfPermission( this, permission ) == PackageManager.PERMISSION_GRANTED) {

                } else {
                }
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (dialog != null) dialog.dismiss();
        dialog = null;
    }
}
