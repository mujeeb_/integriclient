package com.integrimarine.integriclient.Activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.database.sqlite.SQLiteConstraintException;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.integrimarine.integriclient.Database.DatabaseSingleton;
import com.integrimarine.integriclient.Database.Models.CUMediaFileModel;
import com.integrimarine.integriclient.Database.Models.CUMediaScreenModel;
import com.integrimarine.integriclient.Database.Models.CUMediaScreenScheduleModel;
import com.integrimarine.integriclient.Models.CUMediaScreenScheduleFileModel;
import com.integrimarine.integriclient.Models.FileListName;
import com.integrimarine.integriclient.R;
import com.integrimarine.integriclient.Services.JSONHttp;
import com.integrimarine.integriclient.Utils.Config;
import com.integrimarine.integriclient.Utils.SDCardChecker;
import com.integrimarine.integriclient.Fonts.RajdhaniMedium;
import com.integrimarine.integriclient.Fonts.RajdhaniRegular;
import com.integrimarine.integriclient.Utils.Utils;

import org.json.JSONArray;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class DownloadActivity extends AppCompatActivity {

    Context mContext;
    ProgressBar mProgressbar;
    RajdhaniMedium mDownloadBtn;
    RajdhaniRegular mStatus;
    ProgressBar mProgressBarBeforeSyncing;
    CardView mCardViewOfDownloadInfo;
    private List<CUMediaScreenModel> mMediaScreens;
    private List<CUMediaScreenScheduleFileModel> mMediaScreenScheduleFile;
    private List<CUMediaFileModel> mMediaFileModels;

    @SuppressLint("ResourceAsColor")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download);
        mContext = this;
        mDownloadBtn = findViewById(R.id.downloadInfo);
        mProgressbar = findViewById(R.id.progress_horizontal_bar);
        mStatus = findViewById(R.id.downloadingFile);
        mProgressBarBeforeSyncing = findViewById(R.id.ProgressBarBeforeSyncing);
        mCardViewOfDownloadInfo = findViewById(R.id.cardViewOfDownloadInfo);
        getMediaScreens();

        mDownloadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                downloadFile();
            }
        });

    }

    private void getMediaScreens() {
        JSONHttp.getInstance(DownloadActivity.this).get("mediascreen", new JSONHttp.JSONHttpResponseListener() {
            @Override
            public void onResult(JSONArray response) {
                Gson Gson = new Gson();
                mMediaScreens = Gson.fromJson(response.toString(), new TypeToken<List<CUMediaScreenModel>>() {
                }.getType());
                for (CUMediaScreenModel cuMediaScreenModel : mMediaScreens) {
                    try {
                        DatabaseSingleton.getInstance(DownloadActivity.this).getDatabaseInterface().cu_media_screens().insert(cuMediaScreenModel);
                    } catch (SQLiteConstraintException exception) {
                        DatabaseSingleton.getInstance(DownloadActivity.this).getDatabaseInterface().cu_media_screens().update(cuMediaScreenModel);
                    }
                }
                getMediaSchedules();
            }

            @Override
            public void onError() {
                mProgressBarBeforeSyncing.setVisibility(View.GONE);
            }
        });
    }

    private void getMediaSchedules() {
        JSONHttp.getInstance(DownloadActivity.this).get("mediascreenschedule", new JSONHttp.JSONHttpResponseListener() {
            @Override
            public void onResult(JSONArray response) {
                mProgressBarBeforeSyncing.setVisibility(View.GONE);
                mCardViewOfDownloadInfo.setVisibility(View.VISIBLE);
                Gson Gson = new Gson();
                mMediaScreenScheduleFile = Gson.fromJson(response.toString(), new TypeToken<List<CUMediaScreenScheduleFileModel>>() {
                }.getType());
                for (CUMediaScreenScheduleFileModel screenScheduleFileModel : mMediaScreenScheduleFile) {
                    CUMediaScreenScheduleModel cuMediaScreenModel = createMediaScheduleModel(screenScheduleFileModel);
                    try {
                        DatabaseSingleton.getInstance(DownloadActivity.this).getDatabaseInterface().cu_media_screen_schedules().insert(cuMediaScreenModel);
                    } catch (SQLiteConstraintException exception) {
                        DatabaseSingleton.getInstance(DownloadActivity.this).getDatabaseInterface().cu_media_screen_schedules().update(cuMediaScreenModel);
                    }
                    try {
                        DatabaseSingleton.getInstance(DownloadActivity.this).getDatabaseInterface().cu_media_files().insert(screenScheduleFileModel.getMediaFile());
                    } catch (SQLiteConstraintException exception) {
                        DatabaseSingleton.getInstance(DownloadActivity.this).getDatabaseInterface().cu_media_files().update(screenScheduleFileModel.getMediaFile());
                    }
                }
            }

            @Override
            public void onError() {
                Toast.makeText( mContext,"No internet connection",Toast.LENGTH_LONG ).show();
            }
        });
    }

    private CUMediaScreenScheduleModel createMediaScheduleModel(CUMediaScreenScheduleFileModel screenScheduleFileModel) {
        CUMediaScreenScheduleModel scheduleModel = new CUMediaScreenScheduleModel();
        scheduleModel.setIs_deleted(screenScheduleFileModel.getIs_deleted());
        scheduleModel.setMedia_screen_schedule_id(screenScheduleFileModel.getMedia_screen_schedule_id());
        scheduleModel.setRef_customer_id(screenScheduleFileModel.getRef_customer_id());
        scheduleModel.setRef_media_file_id(screenScheduleFileModel.getMediaFile().getMedia_file_id());
        scheduleModel.setRef_media_screen_id(screenScheduleFileModel.getRef_media_screen_id());
        scheduleModel.setRef_ship_id(screenScheduleFileModel.getRef_ship_id());
        scheduleModel.setSchedule_end_date(screenScheduleFileModel.getSchedule_end_date());
        scheduleModel.setSchedule_start_date(screenScheduleFileModel.getSchedule_start_date());
        scheduleModel.setScreen_type(screenScheduleFileModel.getScreen_type());
        return scheduleModel;
    }

    private void downloadFile() {
        mMediaFileModels = DatabaseSingleton.getInstance(DownloadActivity.this).getDatabaseInterface().cu_media_files().getAllMediaFiles();
        //Download file
        if (isConnectingToInternet()) {
            String LocalPath = Utils._BASE_PATH;
            File directory = new File(LocalPath);
            if (!directory.exists()) directory.mkdir();
            File[] files = directory.listFiles();
            String[] namesFromFolder = new String[files.length];
            for (int i = 0; i < files.length; i++) {
                namesFromFolder[i] = files[i].getName();
            }

            List<String> listFiles = Arrays.asList(namesFromFolder);
            boolean filesFound = true;
            for (int i = 0; i < mMediaFileModels.size(); i++) {
                if (!listFiles.contains(mMediaFileModels.get(i).getMedia_name())) {
                    filesFound = false;
                    break;
                }
            }
            if (filesFound) {
                mDownloadBtn.setText("No files to Download");
            } else {
                new DownloadFile().execute();

            }

        } else {
            Toast.makeText(mContext, "Oops!! There is no internet connection. Please enable internet connection and try again.", Toast.LENGTH_SHORT).show();
        }
    }


    private class DownloadFile extends AsyncTask<String, Integer, String> {

        int index;
        File apkStorage = null;
        File outputFile = null;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            ((Activity) mContext).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mDownloadBtn.setText("Downloading in progress..");
                    mProgressbar.setVisibility(View.VISIBLE);
                    mProgressbar.setIndeterminate(true);
                    mProgressbar.setMax(100);
                }
            });
        }


        @Override
        protected void onProgressUpdate(final Integer... values) {
            super.onProgressUpdate(values);
            ((Activity) mContext).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mProgressbar.setProgress(values[0]);
                }
            });
        }

        @Override
        protected String doInBackground(String... strings) {

            String LocalPath = Utils._BASE_PATH;
            File directory = new File(LocalPath);
            File[] files = directory.listFiles();
            String[] namesFromFolder = new String[files.length];
            for (int i = 0; i < files.length; i++) {
                namesFromFolder[i] = files[i].getName();

                Log.e("namesda: ", namesFromFolder[i]);
            }
            List<String> list = Arrays.asList(namesFromFolder);

            try {
                for (int i = 0; i < mMediaFileModels.size(); i++) {
                    if (!list.contains(mMediaFileModels.get(i).getMedia_name())) {

                        URL url = new URL(Config._BASE_UPLOAD_URL + mMediaFileModels.get(i).getMedia_type() + '/' + mMediaFileModels.get(i).getUrl());
                        URLConnection urlConnection = url.openConnection();
                        urlConnection.connect();

                        int fileLength = urlConnection.getContentLength();

                        //Get File if SD card is present
                        if (new SDCardChecker().isSDCardPresent()) {

                            apkStorage = new File(Utils._BASE_PATH);
                        } else
                            Toast.makeText(mContext, "Oops!! There is no SD Card.", Toast.LENGTH_SHORT).show();

                        //If File is not present create directory
                        if (!apkStorage.exists()) {
                            apkStorage.mkdir();
                            Log.e("Directory Created.", "created");
                        }
                        FileListName.setNames(mMediaFileModels.get(i).getUrl());

                        outputFile = new File(apkStorage, mMediaFileModels.get(i).getMedia_name());
                        //Create New File if not present
                        if (!outputFile.exists()) {
                            outputFile.createNewFile();
                        }


                        InputStream inputStream = urlConnection.getInputStream();
                        FileOutputStream outputStream = new FileOutputStream(outputFile);

                        byte data[] = new byte[1024];
                        long total = 0;
                        int count;
                        Log.e("doInBackground: ", fileLength + "");

                        ((DownloadActivity) mContext).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mStatus.setText(FileListName.getNames());
                            }
                        });
                        while ((count = inputStream.read(data)) != -1) {
                            total += count;
                            publishProgress((int) (total * 100 / fileLength));
                            outputStream.write(data, 0, count);
                        }
                        outputStream.close();
                        inputStream.close();
                    } else {

                        Log.e("found: ", "file found ");

                    }

                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            ((Activity) mContext).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        mProgressBarBeforeSyncing.setVisibility(View.GONE);
                        mCardViewOfDownloadInfo.setVisibility(View.VISIBLE);
                        mProgressbar.setVisibility(View.GONE);
                        mStatus.setVisibility(View.INVISIBLE);
                        if (outputFile != null) {
                            Calendar calendar = Calendar.getInstance(Locale.getDefault());
                            int hour = calendar.get(Calendar.HOUR_OF_DAY);
                            int minute = calendar.get(Calendar.MINUTE);
                            int day = calendar.get(Calendar.DAY_OF_MONTH);
                            int month = calendar.get(Calendar.MONTH);
                            int year = calendar.get(Calendar.YEAR);
                            Log.e("Month da: ", month + "");
                            String TimeAmPm;
                            if (hour <= 12) {
                                TimeAmPm = "AM";
                            } else {
                                TimeAmPm = "PM";
                            }
                            String Time = hour % 12 + ":" + minute + " " + TimeAmPm;
                            String OnDate = day + "/" + month+1 + "/" + year;
                            SharedPreferences.Editor editor = mContext.getSharedPreferences("LastSyncTimeDate", MODE_PRIVATE).edit();
                            editor.putString("Time", Time + "");
                            editor.putString("Date", OnDate + "");
                            editor.putString("PathDownloaded", outputFile + "");
                            editor.apply();

                            mDownloadBtn.setText("Download Completed");
                        } else {
                            mDownloadBtn.setText("Download Failed");
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    mDownloadBtn.setEnabled(true);
                                    mDownloadBtn.setText("Download Again");
                                }
                            }, 3000);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        mDownloadBtn.setText("Download Failed");
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                mDownloadBtn.setEnabled(true);
                                mDownloadBtn.setText("Download Again");
                            }
                        }, 3000);

                    }


                }
            });
        }

    }


    //Check if internet is present or not
    private boolean isConnectingToInternet() {
        ConnectivityManager connectivityManager = (ConnectivityManager) mContext.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) return true;
        else return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
