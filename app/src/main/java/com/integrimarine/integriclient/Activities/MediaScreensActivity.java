package com.integrimarine.integriclient.Activities;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.widget.Toast;

import com.integrimarine.integriclient.Adapters.MediaScreenAdapter;
import com.integrimarine.integriclient.Database.DatabaseSingleton;
import com.integrimarine.integriclient.Database.Models.CUMediaScreenModel;
import com.integrimarine.integriclient.Fonts.RajdhaniMedium;
import com.integrimarine.integriclient.R;

import java.util.List;

public class MediaScreensActivity extends AppCompatActivity {


    CardView cardViewSyncNow;
    Context mContext;
    MediaScreenAdapter mediaScreenAdapter;
    RecyclerView recyclerView;

    RajdhaniMedium lastSyncTime, lastSyncDate;
    List<CUMediaScreenModel> mMediaScreenModels;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_media_screens);

        mContext = this;

        cardViewSyncNow = findViewById(R.id.cardViewSyncNow);
        lastSyncDate = findViewById(R.id.activity_media_screen_lastSyncDate);
        lastSyncTime = findViewById(R.id.activity_media_screen_lastSyncTime);

        recyclerView = findViewById(R.id.mediaScreenRecyclerView);
        StaggeredGridLayoutManager verticalLayout
                = new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(verticalLayout);

        cardViewSyncNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MediaScreensActivity.this, DownloadActivity.class));
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        //stored previos sync
        SharedPreferences prefs = mContext.getSharedPreferences("LastSyncTimeDate", MODE_PRIVATE);
        String time = prefs.getString("Time", null);
        String date = prefs.getString("Date", null);
        if (time != null) {
            lastSyncTime.setText("Last Sync time :" + time);
            lastSyncDate.setText("Last Sync Date :" + date);
        }
        mMediaScreenModels = DatabaseSingleton.getInstance(this).getDatabaseInterface().cu_media_screens().getAllShips();
        mediaScreenAdapter = new MediaScreenAdapter(this, mMediaScreenModels);
        recyclerView.setAdapter(mediaScreenAdapter);
        if (mMediaScreenModels.size() == 0) {
            startActivity(new Intent(MediaScreensActivity.this, DownloadActivity.class));
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

}
