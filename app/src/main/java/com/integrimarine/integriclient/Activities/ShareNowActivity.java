package com.integrimarine.integriclient.Activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import com.integrimarine.integriclient.Database.DatabaseSingleton;
import com.integrimarine.integriclient.Database.Models.CUMediaFileModel;
import com.integrimarine.integriclient.Database.Models.CUMediaScreenScheduleModel;
import com.integrimarine.integriclient.Fonts.RajdhaniRegular;
import com.integrimarine.integriclient.Models.CUMediaScreenScheduleFileModel;
import com.integrimarine.integriclient.R;
import com.integrimarine.integriclient.Utils.Config;
import com.integrimarine.integriclient.Utils.Utils;

import java.io.BufferedInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ShareNowActivity extends AppCompatActivity {

    int i = 0;
    static final int SocketServerPORT = 8888;
    ServerSocket serverSocket;
    ProgressDialog mProgressDialog;
    CardView shareNow;
    RajdhaniRegular shareNowTextView;
    OutputStream outputStream;
    String fileNameRecieved;
    Context mContext;
    ServerSocketThread serverSocketThread;
    WifiManager wifiManager;
    private String mMediaScreenId;

    private List<CUMediaFileModel> mMediaFileModels;
    private List<String> mSharableMediaFiles = new ArrayList<>();
    private List<CUMediaScreenScheduleModel> mMediaScreenScheduleModels;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_now);
        mContext = this;

        //initialize each id
        ImageView imageGIF = findViewById(R.id.activity_fragment_shareNow_ImageGIF);
        shareNow = findViewById(R.id.activity_fragment_CardView_Share_Now);
        shareNowTextView = findViewById(R.id.activity_fragment_shareNow_shareNowTextview);

        mMediaScreenId = getIntent().getStringExtra("media_screen_id");
        if (mMediaScreenId == null) finish();
        mMediaFileModels = DatabaseSingleton.getInstance(ShareNowActivity.this).getDatabaseInterface().cu_media_files().findMediaFilesByMediaScreen(mMediaScreenId);
        mMediaScreenScheduleModels = DatabaseSingleton.getInstance(ShareNowActivity.this).getDatabaseInterface().cu_media_screen_schedules().findMediaScreenSchedulesByScreenId(mMediaScreenId);
        //display gif
        Glide.with(this).load(R.mipmap.hotspot).into(imageGIF);

        //initialise wifimanager to configure wifi and htspot
        wifiManager = (WifiManager) mContext.getApplicationContext().getSystemService(WIFI_SERVICE);

        //Connectecting to another Device
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            try {
                if (checkSystemWritePermission()) {
                    shareNowTextView.setText("Click receive in Tab app to share files");
                    setWifiTetheringEnabled(true);
                }
            } catch (Exception e) {
            }
        } else {
            setWifiTetheringEnabled(true);
        }
        File directory = new File(Utils._BASE_PATH);
        File[] files = directory.listFiles();
        String[] namesFromFolder = new String[files.length];
        for (int i = 0; i < files.length; i++) {
            namesFromFolder[i] = files[i].getName();
        }

        List<String> listFiles = Arrays.asList(namesFromFolder);

        for (int i = 0; i < mMediaFileModels.size(); i++) {
            if (listFiles.contains(mMediaFileModels.get(i).getMedia_name())) {
                mSharableMediaFiles.add(mMediaFileModels.get(i).getMedia_name());
            }
        }
        //run server socket to send files
        serverSocketThread = new ServerSocketThread();
        serverSocketThread.start();

    }


    public class ServerSocketThread extends Thread {

        @Override
        public void run() {
            Socket socket = null;

            try {
                serverSocket = new ServerSocket(SocketServerPORT);
                ((Activity) mContext).runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        Toast.makeText(mContext, "Hotspot created", Toast.LENGTH_LONG).show();
                    }
                });

                while (true) {
                    socket = serverSocket.accept();
                    FileTxThread fileTxThread = new FileTxThread(socket);
                    fileTxThread.execute();

                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } finally {
                if (socket != null) {
                    try {
                        socket.close();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
        }

    }

    public class FileTxThread extends AsyncTask<String, Integer, String> {
        Socket socket;


        FileTxThread(Socket socket) {
            this.socket = socket;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            ((Activity) mContext).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mProgressDialog = new ProgressDialog(mContext);
                    mProgressDialog.setMessage("Sending File..");
                    mProgressDialog.setIndeterminate(false);
                    mProgressDialog.setMax(100);
                    mProgressDialog.setCancelable(false);
                    mProgressDialog.setCanceledOnTouchOutside(false);
                    mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                    mProgressDialog.show();
                }
            });

        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            mProgressDialog.setProgress(values[0]);
        }

        @Override
        protected String doInBackground(String... strings) {


            int count;
            byte[] buffer = new byte[8192]; // or 4096, or more

            BufferedInputStream bufferedInputStream;
            DataOutputStream dataOutputStream;

            try {
                Gson gson = new Gson();
                String mediaScreenSchedules = gson.toJson(mMediaScreenScheduleModels, new TypeToken<List<CUMediaScreenScheduleModel>>() {
                }.getType());
                String mediaFiles = gson.toJson(mMediaFileModels, new TypeToken<List<CUMediaFileModel>>() {
                }.getType());
                Log.e( "doInBackground: ", mediaScreenSchedules);
                Log.e( "doInBackground: ", mediaFiles);
                outputStream = socket.getOutputStream();
                dataOutputStream = new DataOutputStream(socket.getOutputStream());
                dataOutputStream.writeUTF(mediaScreenSchedules);
                dataOutputStream.flush();
                dataOutputStream.writeUTF(mediaFiles);
                dataOutputStream.flush();
                dataOutputStream.writeUTF(mSharableMediaFiles.size() + "");
                dataOutputStream.flush();
                for (i = 0; i < mSharableMediaFiles.size(); i++) {
                    String EachfilePath = Utils._BASE_PATH + "/" + mSharableMediaFiles.get(i);
                    Log.e("Each Path: ", EachfilePath + "");
                    Log.e("doInBackground: ", i + "");
                    File file = new File(EachfilePath + "");
                    bufferedInputStream = new BufferedInputStream(new FileInputStream(EachfilePath));
                    Long fileLength = file.length();
                    String fileLengthString = fileLength.toString();
                    fileNameRecieved = mSharableMediaFiles.get(i);
                    Log.e("Name: ", fileNameRecieved);

                    dataOutputStream.writeUTF(fileNameRecieved);
                    dataOutputStream.flush();
                    dataOutputStream.writeUTF(fileLengthString);
                    dataOutputStream.flush();
                    long total = 0;

                    while ((count = bufferedInputStream.read(buffer)) != -1) {
                        total += count;
                        publishProgress((int) (total * 100 / fileLength));
                        outputStream.write(buffer, 0, count);
                    }
                    outputStream.flush();
                    dataOutputStream.writeUTF("success");
                    dataOutputStream.flush();
                }
                dataOutputStream.close();
                outputStream.close();
                socket.close();

                final String sentMsg = "File sent ";
                ((Activity) mContext).runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        Toast.makeText(mContext, sentMsg, Toast.LENGTH_LONG).show();
                    }
                });

            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } finally {
                try {
                    socket.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            mProgressDialog.dismiss();
            shareNowTextView.setText("Files Sent");
        }
    }


    //Enable Hotspot
    public void setWifiTetheringEnabled(boolean enable) {


        WifiManager wifiManager = (WifiManager) mContext.getApplicationContext().getSystemService(WIFI_SERVICE);

        if (enable) {
            wifiManager.setWifiEnabled(!enable);    // Disable all existing WiFi Network
        } else {
            if (!wifiManager.isWifiEnabled()) wifiManager.setWifiEnabled(!enable);
        }
        Method[] methods = wifiManager.getClass().getDeclaredMethods();
        for (Method method : methods) {
            if (method.getName().equals("setWifiApEnabled")) {
                WifiConfiguration netConfig = new WifiConfiguration();
                if (!Config.SSID.isEmpty()) {
                    netConfig.SSID = Config.SSID;
                    netConfig.hiddenSSID = false;
                    netConfig.status = WifiConfiguration.Status.ENABLED;
                    netConfig.allowedKeyManagement.set( WifiConfiguration.KeyMgmt.NONE );
                    netConfig.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
                    netConfig.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
                    netConfig.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
                    netConfig.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
                    netConfig.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
                }
                try {
                    method.invoke(wifiManager, netConfig, enable);

                } catch (Exception ex) {
                }
                break;
            }
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        if (serverSocket != null) {
            try {
                serverSocket.close();
                disableHospot();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

    }


    private boolean checkSystemWritePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (Settings.System.canWrite(mContext)) return true;
            else openAndroidPermissionsMenu();
        }
        return false;
    }

    private void openAndroidPermissionsMenu() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
            intent.setData(Uri.parse("package:" + mContext.getPackageName()));
            mContext.startActivity(intent);
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        disableHospot();
    }


    private void disableHospot() {
        try {

            WifiManager wifiManager = (WifiManager) mContext.getApplicationContext().getSystemService(WIFI_SERVICE);
            WifiConfiguration wifi_configuration = new WifiConfiguration();
            //USE REFLECTION TO GET METHOD "SetWifiAPEnabled"
            Method method = wifiManager.getClass().getMethod("setWifiApEnabled", WifiConfiguration.class, boolean.class);
            method.invoke(wifiManager, wifi_configuration, false);
        } catch (NoSuchMethodException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
    }

}
