package com.integrimarine.integriclient.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.integrimarine.integriclient.Adapters.FileListingAdapter;
import com.integrimarine.integriclient.Database.DatabaseSingleton;
import com.integrimarine.integriclient.Database.Models.CUMediaFileModel;
import com.integrimarine.integriclient.R;
import com.integrimarine.integriclient.Utils.SDCardChecker;
import com.integrimarine.integriclient.Fonts.RajdhaniMedium;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

public class SyncNowActivity extends AppCompatActivity {

    RajdhaniMedium syncNow, lastSyncTime, lastSyncDate;
    int screenNo;
    FileListingAdapter adapter;
    RecyclerView recyclerView;
    Context context;
    CardView shareNow, ButtonView_syncNow;
    RelativeLayout reative_layout_empty_folder;


    File storage = null;
    private String mMediaScreenId;
    private List<CUMediaFileModel> mMediaFiles;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sync_now);
        context = this;
        /**
         * Which Screen id Selected
         */
        mMediaScreenId = getIntent().getStringExtra("media_screen_id");
        if (mMediaScreenId == null) finish();
        syncNow = findViewById(R.id.syncNow);
        lastSyncDate = findViewById(R.id.lastSyncDate);
        lastSyncTime = findViewById(R.id.lastSyncTime);
        shareNow = findViewById(R.id.shareNow);
        ButtonView_syncNow = findViewById(R.id.syncNowWhenFileIsEmpty);
        reative_layout_empty_folder = findViewById(R.id.reative_layout_empty_folder);

        syncNow.setVisibility(View.VISIBLE);
        shareNow.setVisibility(View.VISIBLE);
        ButtonView_syncNow.setVisibility(View.VISIBLE);

        shareNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ShareNowActivity.class);
                intent.putExtra("media_screen_id", mMediaScreenId);
                startActivity(intent);
            }
        });

        //Get File if SD card is present
        if (new SDCardChecker().isSDCardPresent()) {

            storage = new File(Environment.getExternalStorageDirectory() + "/shipVideo/");
        } else Toast.makeText(context, "Oops!! There is no SD Card.", Toast.LENGTH_SHORT).show();
        if (!storage.exists()) {
            storage.mkdir();
            Log.e("Created: ", "creted");
        }
        //stored previos sync
        SharedPreferences prefs = context.getSharedPreferences("LastSyncTimeDate", MODE_PRIVATE);
        String time = prefs.getString("Time", null);
        String date = prefs.getString("Date", null);

        if (time != null) {
            lastSyncTime.setText("Last Sync time :" + time);
            lastSyncDate.setText("Last Sync Date :" + date);
        }
        /**
         * syncNow and Button Both Moves to Download Activity
         */
        syncNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SyncNowActivity.this, DownloadActivity.class));
            }
        });
        ButtonView_syncNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SyncNowActivity.this, DownloadActivity.class));
            }
        });


        disableHospot();
        recyclerView = findViewById(R.id.mediaPlayerRecyclerView);
        StaggeredGridLayoutManager verticalLayout
                = new StaggeredGridLayoutManager(5, StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(verticalLayout);

    }


    @Override
    protected void onResume() {
        super.onResume();
        mMediaFiles = DatabaseSingleton.getInstance(SyncNowActivity.this).getDatabaseInterface().cu_media_files().findMediaFilesByMediaScreen(mMediaScreenId);
        adapter = new FileListingAdapter(this, mMediaFiles);
        recyclerView.setAdapter(adapter);
        if (mMediaFiles.size() == 0) {
            reative_layout_empty_folder.setVisibility(View.VISIBLE);
        }
        disableHospot();
    }

    @Override
    protected void onPause() {
        super.onPause();
        disableHospot();
    }

    private void disableHospot() {
        try {

            WifiManager wifiManager = (WifiManager) context.getApplicationContext().getSystemService(WIFI_SERVICE);
            WifiConfiguration wifi_configuration = new WifiConfiguration();
            //USE REFLECTION TO GET METHOD "SetWifiAPEnabled"
            Method method = wifiManager.getClass().getMethod("setWifiApEnabled", WifiConfiguration.class, boolean.class);
            method.invoke(wifiManager, wifi_configuration, false);
        } catch (NoSuchMethodException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
