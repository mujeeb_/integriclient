package com.integrimarine.integriclient.Database;

import android.arch.persistence.room.Room;
import android.content.Context;


public class DatabaseSingleton {
    private final Context mContext;
    public static DatabaseSingleton mInstance;
    private DatabaseInterface databaseInterface;

    public DatabaseSingleton(Context context){
        mContext = context;
        databaseInterface = Room.databaseBuilder(mContext,DatabaseInterface.class,"integri_db").allowMainThreadQueries().build();
    }

    public DatabaseInterface getDatabaseInterface() {
        return databaseInterface;
    }

    public static synchronized DatabaseSingleton getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new DatabaseSingleton( context );
        }
        return mInstance;
    }
}
