package com.integrimarine.integriclient.Database.Interfaces;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.integrimarine.integriclient.Database.Models.CUMediaScreenModel;

import java.util.List;

@Dao
public interface CUMediaScreenInterface {

    @Query( "SELECT * FROM cu_media_screens" )
    public abstract List<CUMediaScreenModel> getAllShips();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    public long insert(CUMediaScreenModel screenModel);

    @Update(onConflict = OnConflictStrategy.IGNORE)
    public void update(CUMediaScreenModel screenModel);
}
