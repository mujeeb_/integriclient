package com.integrimarine.integriclient.Database.Models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.support.annotation.NonNull;

import com.integrimarine.integriclient.Database.Util.DatatypeConverter;

import java.util.Date;

@Dao
@Entity(tableName = "cu_media_screen_schedules")
public class CUMediaScreenScheduleModel {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "media_screen_schedule_id")
    private String media_screen_schedule_id;

    @ColumnInfo(name = "ref_customer_id")
    private String ref_customer_id;

    @ColumnInfo(name = "ref_media_file_id")
    private String ref_media_file_id;

    @ColumnInfo(name = "ref_media_screen_id")
    private String ref_media_screen_id;


    @ColumnInfo(name = "ref_ship_id")
    private String ref_ship_id;

    @ColumnInfo(name = "screen_type")
    private String screen_type;


    @ColumnInfo(name = "schedule_start_date")
    @TypeConverters(DatatypeConverter.class)
    private Date schedule_start_date;

    @ColumnInfo(name = "schedule_end_date")
    @TypeConverters(DatatypeConverter.class)
    private Date schedule_end_date;

    @ColumnInfo(name = "is_deleted")
    private Boolean is_deleted;

    @NonNull
    public String getMedia_screen_schedule_id() {
        return media_screen_schedule_id;
    }

    public void setMedia_screen_schedule_id(@NonNull String media_screen_schedule_id) {
        this.media_screen_schedule_id = media_screen_schedule_id;
    }

    public String getRef_customer_id() {
        return ref_customer_id;
    }

    public void setRef_customer_id(String ref_customer_id) {
        this.ref_customer_id = ref_customer_id;
    }

    public String getRef_media_file_id() {
        return ref_media_file_id;
    }

    public void setRef_media_file_id(String ref_media_file_id) {
        this.ref_media_file_id = ref_media_file_id;
    }

    public String getRef_media_screen_id() {
        return ref_media_screen_id;
    }

    public void setRef_media_screen_id(String ref_media_screen_id) {
        this.ref_media_screen_id = ref_media_screen_id;
    }

    public String getRef_ship_id() {
        return ref_ship_id;
    }

    public void setRef_ship_id(String ref_ship_id) {
        this.ref_ship_id = ref_ship_id;
    }

    public String getScreen_type() {
        return screen_type;
    }

    public void setScreen_type(String screen_type) {
        this.screen_type = screen_type;
    }

    public Date getSchedule_start_date() {
        return schedule_start_date;
    }

    public void setSchedule_start_date(Date schedule_start_date) {
        this.schedule_start_date = schedule_start_date;
    }

    public Date getSchedule_end_date() {
        return schedule_end_date;
    }

    public void setSchedule_end_date(Date schedule_end_date) {
        this.schedule_end_date = schedule_end_date;
    }

    public Boolean getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(Boolean is_deleted) {
        this.is_deleted = is_deleted;
    }
}
