package com.integrimarine.integriclient.Database.Interfaces;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.integrimarine.integriclient.Database.Models.CUMediaScreenScheduleModel;

import java.util.List;

@Dao
public interface CUMediaScreenScheduleInterface {

    @Query( "SELECT * FROM cu_media_screen_schedules" )
    public abstract List<CUMediaScreenScheduleModel> getAllScreenSchedules();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    public long insert(CUMediaScreenScheduleModel screenScheduleModel);

    @Update(onConflict = OnConflictStrategy.IGNORE)
    public void update(CUMediaScreenScheduleModel screenScheduleModel);

    @Query( "SELECT * FROM cu_media_screen_schedules" )
    public abstract List<CUMediaScreenScheduleModel> getAllMediaScreenSchudules();

    @Query("SELECT * FROM cu_media_screen_schedules WHERE ref_media_screen_id = :media_screen_id ")
    public abstract List<CUMediaScreenScheduleModel> findMediaScreenSchedulesByScreenId(String media_screen_id);
}
