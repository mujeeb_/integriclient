package com.integrimarine.integriclient.Database.Interfaces;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.integrimarine.integriclient.Database.Models.CUMediaFileModel;
import com.integrimarine.integriclient.Database.Models.CUMediaScreenModel;
import com.integrimarine.integriclient.Database.Models.CUMediaScreenScheduleModel;

import java.util.List;

@Dao
public interface CUMediaFileInterface {

    @Query( "SELECT * FROM cu_media_files" )
    public abstract List<CUMediaFileModel> getAllMediaFiles();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    public long insert(CUMediaFileModel mediaFileModel);

    @Update(onConflict = OnConflictStrategy.IGNORE)
    public void update(CUMediaFileModel mediaFileModel);

    @Query("SELECT * FROM cu_media_files WHERE media_file_id IN ( SELECT ref_media_file_id FROM cu_media_screen_schedules WHERE ref_media_screen_id = :media_screen)")
    public abstract List<CUMediaFileModel> findMediaFilesByMediaScreen(String media_screen);

}
