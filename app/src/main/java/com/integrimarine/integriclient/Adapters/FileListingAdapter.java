package com.integrimarine.integriclient.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.integrimarine.integriclient.Database.Models.CUMediaFileModel;
import com.integrimarine.integriclient.R;
import com.integrimarine.integriclient.Utils.Utils;

import java.util.List;


public class FileListingAdapter extends RecyclerView.Adapter<FileListingAdapter.ViewHolder> {

    private Context context;
    private List<CUMediaFileModel> mCUMediaFileModels;

    public FileListingAdapter(Context context, List<CUMediaFileModel> cuMediaFileModels) {
        this.context = context;
        mCUMediaFileModels = cuMediaFileModels;
    }

    public int getItemViewType(int position) {
        return R.layout.files_listing_layout;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new FileListingAdapter.ViewHolder(LayoutInflater.from(context).inflate(i, viewGroup, false));

    }

    @Override
    public void onBindViewHolder(FileListingAdapter.ViewHolder holder, int position) {
        holder.bindData(holder, position);
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView fileListing;

        public ViewHolder(View itemView) {
            super(itemView);
            fileListing = itemView.findViewById(R.id.filesListing);
        }

        public void bindData(ViewHolder view, int position) {
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.mipmap.pdf);
            Glide.with(context)
                    .load(Utils._BASE_PATH + "/" + mCUMediaFileModels.get(getAdapterPosition()).getMedia_name()
                            )
                    .apply(requestOptions)
                    .into(view.fileListing);

        }


    }

    @Override
    public int getItemCount() {
        if (mCUMediaFileModels == null) return 0;
        return mCUMediaFileModels.size();
    }


}