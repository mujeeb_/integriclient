package com.integrimarine.integriclient.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.integrimarine.integriclient.Activities.SyncNowActivity;
import com.integrimarine.integriclient.Database.Models.CUMediaScreenModel;
import com.integrimarine.integriclient.R;
import com.integrimarine.integriclient.Fonts.RajdhaniMedium;

import java.util.List;

public class MediaScreenAdapter extends RecyclerView.Adapter<MediaScreenAdapter.ViewHolder> {

    private Context mContext;
    private List<CUMediaScreenModel> mMediaScreenModels;

    public MediaScreenAdapter(Context context, List<CUMediaScreenModel> mediaScreenModels) {
        this.mContext = context;
        mMediaScreenModels = mediaScreenModels;
    }

    public int getItemViewType(int position) {
        return R.layout.media_screen_layout;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new MediaScreenAdapter.ViewHolder(LayoutInflater.from(mContext).inflate(i, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(MediaScreenAdapter.ViewHolder holder, int position) {
        holder.bindData();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView mediaScreenImageView;
        RajdhaniMedium mediaScreenCount;

        public ViewHolder(View itemView) {
            super(itemView);
            mediaScreenImageView = itemView.findViewById(R.id.screenImage);
            mediaScreenCount = itemView.findViewById(R.id.screenId_TextView);
        }

        public void bindData() {
            mediaScreenCount.setText(mMediaScreenModels.get(getAdapterPosition()).getName());
            mediaScreenImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, SyncNowActivity.class);
                    intent.putExtra("media_screen_id", mMediaScreenModels.get(getAdapterPosition()).getMedia_screen_id());
                    mContext.startActivity(intent);
                }
            });

        }

    }

    @Override
    public int getItemCount() {
        return mMediaScreenModels.size();
    }


}