package com.integrimarine.integriclient.Models;


import com.integrimarine.integriclient.Database.Models.CUMediaFileModel;
import com.integrimarine.integriclient.Database.Models.CUMediaScreenScheduleModel;


public class CUMediaScreenScheduleFileModel extends CUMediaScreenScheduleModel {
    private CUMediaFileModel mediaFile;

    public CUMediaFileModel getMediaFile() {
        return mediaFile;
    }

    public void setMediaFile(CUMediaFileModel mediaFile) {
        this.mediaFile = mediaFile;
    }

//    @Override
//    public Date getScheduled_start_date() {
//        return scheduled_start_date;
//    }
//
//    public void setScheduled_start_date(String scheduled_start_date) {
//        DateFormat jsDateFormat = new SimpleDateFormat("EE MMM d y H:m:s 'GMT'Z (zz)");
//        try {
//            this.scheduled_start_date = jsDateFormat.parse(scheduled_start_date);
//            Log.e( "setScheduled_date: ", scheduled_start_date);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//    }
//
//    @Override
//    public Date getScheduled_end_date() {
//        return scheduled_end_date;
//    }
//
//    public void setScheduled_end_date(String scheduled_end_date) {
//        DateFormat jsDateFormat = new SimpleDateFormat("EE MMM d y H:m:s 'GMT'Z (zz)");
//        try {
//            this.scheduled_end_date = jsDateFormat.parse(scheduled_end_date);
//            Log.e("setScheduled_end_date: ", scheduled_start_date.toString());
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//    }
}
