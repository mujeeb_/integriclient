package com.integrimarine.integriclient.Models;

import android.util.Log;

public class FileListName {
    private static String names;

    public static String getNames() {
        return names;
    }

    public static void setNames(String names) {
        FileListName.names = names;
        Log.e( "setNames: ", names );
    }
}
