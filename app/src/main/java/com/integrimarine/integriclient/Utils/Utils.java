package com.integrimarine.integriclient.Utils;

import android.os.Environment;

import java.io.File;

public class Utils {
    public static final String downloadDirectory = "ShipVideo";
    //public static final String downloadVideoUrl[] = {"https://sample-videos.com/video123/mp4/360/big_buck_bunny_360p_2mb.mp4","https://sample-videos.com/video123/mp4/360/big_buck_bunny_360p_5mb.mp4","https://sample-videos.com/video123/mp4/360/big_buck_bunny_360p_20mb.mp4","https://sample-videos.com/video123/mp4/480/big_buck_bunny_480p_2mb.mp4"};
    public static final String extensionForVideo[] = {".mp4",".mkv",".webm",".flv",".avi",".yuv",".mpeg"};
    public static final String extensionForImage[] = {".png",".jpg",".jpeg"};
    public static final String extensionForPDF = ".pdf";

    public static String _BASE_PATH = Environment.getExternalStorageDirectory().getAbsolutePath()+"/shipVideo";
    public static File directory = new File(_BASE_PATH);
    //public static File[] files = directory.listFiles();
}
