package com.integrimarine.integriclient.Utils;

import android.os.Environment;

public class SDCardChecker {
    public boolean isSDCardPresent() {
        if (Environment.getExternalStorageState().equals(

                Environment.MEDIA_MOUNTED)) {
            return true;
        }
        return false;
    }
}
