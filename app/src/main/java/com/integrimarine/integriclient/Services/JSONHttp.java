package com.integrimarine.integriclient.Services;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.integrimarine.integriclient.Utils.Config;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class JSONHttp {
    private Context mContext;
    private static JSONHttp mInstance;

    public JSONHttp(Context context) {
        mContext = context;
    }

    public interface JSONHttpResponseListener {
        void onResult(JSONArray response);

        void onError();
    }

    public void get(String url, final JSONHttpResponseListener responseListener) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest( Request.Method.GET, Config._BASE_URL + url, null, new Response.Listener <JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (responseListener != null) {
                    try {
                        responseListener.onResult(response.getJSONArray( "data" ));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (responseListener != null) {
                    responseListener.onError();
                }
            }
        } ) {
            @Override
            public Map <String, String> getHeaders() throws AuthFailureError {
                Map <String, String> params = new HashMap <String, String>();
                params.put( "Content-Type", "application/json" );
                params.put( "token", SharedPreferenceService.getInstance( mContext ).getToken() );
                return params;
            }
        };

        MySingleton.getInstance( mContext ).addToRequestQueue( jsonObjectRequest );
    }

    public static synchronized JSONHttp getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new JSONHttp( context );
        }
        return mInstance;
    }

    public void login(final String token, final JSONHttpResponseListener responseListener) {
        JSONObject body = new JSONObject();
        try {
            body.put( "token", token );
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest( Request.Method.POST, Config._BASE_URL + "auth/login", body, new Response.Listener <JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    String token = response.getString( "token" );
                    SharedPreferenceService.getInstance( mContext ).setToken( token );
                    if (responseListener != null) {
                        responseListener.onResult( null );
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                if (responseListener != null) {
                    responseListener.onError();
                }
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null && networkResponse.data != null) {
                    String jsonError = new String( networkResponse.data );
                    Log.e( "onErrorResponse: ", jsonError );
//                    if (responseListener != null && networkResponse.statusCode == 400) {
//                        responseListener.onError();
//                    }
                }

            }
        } ) {
            @Override
            public Map <String, String> getHeaders() throws AuthFailureError {
                Map <String, String> params = new HashMap <String, String>();
                params.put( "Content-Type", "application/json" );
                return params;
            }
        };

        MySingleton.getInstance( mContext ).addToRequestQueue( jsonObjectRequest );
    }
}

