package com.integrimarine.integriclient.Services;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

public class    SharedPreferenceService {

    private Context mContext;
    public static SharedPreferenceService mInstance;
    private static final String MY_PREFS_NAME = "INTEGRI_TAB_APP";

    public SharedPreferenceService(Context  context){
        mContext = context;
    }

    public void setToken(String token){
        SharedPreferences.Editor editor = mContext.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putString("ship_token", token);
        editor.apply();
    }

    public String getToken(){
        SharedPreferences prefs = mContext.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        String token = prefs.getString("ship_token", null);
        return token;
    }

    public static synchronized SharedPreferenceService getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new SharedPreferenceService(context);
        }
        return mInstance;
    }
}
